from sqlalchemy import Column, Integer, Text, String, ForeignKey
from sqlalchemy.orm import relationship
from . import Base


class CategoryLang(Base):
    __tablename__ = 'category_lang'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    lang_id = Column(Integer, ForeignKey('lang.id', ondelete='CASCADE'), nullable=False)
    parent_id = Column(Integer, ForeignKey('category.id', ondelete='CASCADE'), nullable=False)

    category_lang = relationship('Lang', back_populates='category_lang')
    category = relationship('Category', back_populates='category')