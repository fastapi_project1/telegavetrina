from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from . import Base

import sqlalchemy

class User(Base):


    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    phone = Column(String,unique=True,index=True)
    email = Column(String)
    name = Column(String)
    hashed_password=Column(String)
    is_admin= Column(sqlalchemy.Boolean())
    is_active = Column(sqlalchemy.Boolean())
    code_phone = Column(String,index=True)
    short_phone = Column(String)
