from fastapi.testclient import TestClient
from main import app

client = TestClient(app)
emailtest="test19992@test"
password="pasworddd2123test"

def test_read_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Test app GT"}

def test_auth_register():
    response = client.post(
        "/auth/signup",
        headers={"X-Token": "coneofsilence"},
        json={"emailuser": emailtest, "username": "Test user", "password": password},
    )
    assert response.status_code == 200
    assert response.json() == {
        "status": "ok", "detail": "User create"
    }

def test_auth_register_duplicat():
    response = client.post(
        "/auth/signup",
        headers={"X-Token": "coneofsilence"},
        json={"emailuser": emailtest, "username": "Test user", "password": password},
    )
    assert response.status_code == 500
    assert response.json() == {
        "detail": "ok", "detail": "Failed  signup"
    }

def test_auth_login():
    response = client.post(
        "/auth/login",
        headers={"X-Token": "coneofsilence"},
        json={"emailuser": emailtest, "password": password},
    )
    assert response.status_code == 200

def test_auth_login_error():
    response = client.post(
        "/auth/login",
        headers={"X-Token": "coneofsilence"},
        json={"emailuser": emailtest+"1", "password": password+"1"},
    )
    assert response.status_code == 404
    assert response.json() == {
        "detail": "Not found record"
    }

def test_auth_login_error_notpassword():
    response = client.post(
        "/auth/login",
        headers={"X-Token": "coneofsilence"},
        json={"emailuser": emailtest, "password": password+"1"},
    )
    assert response.status_code == 500
    assert response.json() == {
        "detail": "Failed  login"
    }