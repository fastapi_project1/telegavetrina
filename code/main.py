from fastapi import FastAPI
from apps.auth.routers import router as auth_router
from apps.api.routers import router as api_router
from apps.crud.routers import router as crud_router

# Создание экземпляра FastAPI
app = FastAPI(
    title="Back Api telega_vetrina",
    description="Description",
    version="1.0.2",
)

@app.get("/")
async def root():
    return {"message": "Test app GT"}


app.include_router(auth_router, prefix="/auth")
app.include_router(api_router, prefix="/api")
app.include_router(crud_router, prefix="/crud")