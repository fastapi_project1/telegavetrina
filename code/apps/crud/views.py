from fastapi import HTTPException
from models.user import User
from models.lang import Lang
from models.category import Category
from models.category_lang import CategoryLang
from models.htmlblock import Htmlblock
from models.htmlblock_lang import HtmlblockLang

def fabricaModel(nameObject:str):
    match nameObject:
        case "langs":
            objModel = Lang
        case "users":
            objModel = User
        case "categorys":
            objModel = Category
        case "categorys_lang":
            objModel = CategoryLang
        case "htmlblocks":
            objModel = Htmlblock
        case "htmlblocks_lang":
            objModel = HtmlblockLang
        case _:
            raise HTTPException(status_code=404, detail="Not table")
    return objModel


class CrudView:

    # конструктор
    def __init__(self, db ,phone):
        self.db = db
        self.phone = phone
        self.getIsAdmin()

    # проверяем имя
    def getIsAdmin(self):
        db_user = self.db.query(User).filter(User.phone == self.phone).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        if (db_user.is_admin):
            return db_user
        raise HTTPException(status_code=404, detail="Not admin")

    #Получаем данные с таблицы по имени
    def getlist(self,name: str,skip: int = 0, limit: int = 10):
        db_session = self.db
        objectData=fabricaModel(name)
        apply_offset = skip and skip > 0
        # Запрос
        db_records = self.db.query(objectData)
        if apply_offset:
            offset = (skip - 1) * limit
            db_records = db_records.offset(offset).limit(limit)
        else:
            db_records = db_records.limit(limit)
        # запрос к базе и получаем данные
        db_records = db_records.all()
        db_session.close()
        # выводим данные
        if db_records is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        for val in db_records:
            resultArray.append(val)
        return resultArray

    def getshow(self,name: str, id: int):
        db_session = self.db
        objModel = fabricaModel(name)
        db_data = self.db.query(objModel).filter(objModel.id == id).first()
        db_session.close()
        if db_data is None:
            raise HTTPException(status_code=404, detail="Not record id in table:" + name)
        return db_data