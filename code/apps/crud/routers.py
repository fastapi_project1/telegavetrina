from fastapi import APIRouter, Depends, Form
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from jose import jwt,JWTError
from dotenv import load_dotenv
from pathlib import Path
from datetime import datetime, timedelta
from apps.crud.views import CrudView
from database import SessionLocal
import os

router = APIRouter()
env_path =  '/gtapp/.env'
load_dotenv(dotenv_path=env_path)
# Configure JWT settings
SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = os.getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES = 15
REFRESH_TOKEN_EXPIRE_DAYS = 7


router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
       payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
       #Тут делаем проверку на то что он админ
       return payload.get("sub")
    except jwt.ExpiredSignatureError:
       print("тут какая-то логика ошибки истечения срока действия токена")
       pass  # тут какая-то логика ошибки истечения срока действия токена
    except jwt.InvalidTokenError:
       raise HTTPException(status_code=401, detail="Invalid token")

@router.post("/list/{tablename}")
def post_auth_login(tablename: str,current_user: str = Depends(get_user_from_token)):
    # Получаем сессию  БД
    session = SessionLocal()
    # конструктор представления
    crud_view = CrudView(session,current_user)
    listData = crud_view.getlist(tablename)
    return listData
@router.post("/show/{tablename}/{id}")
def post_show_id(tablename: str,id :int,current_user: str = Depends(get_user_from_token)):
    # Получаем сессию  БД
    session = SessionLocal()
    # конструктор представления
    crud_view = CrudView(session, current_user)
    showData = crud_view.getshow(tablename,id)
    return  showData